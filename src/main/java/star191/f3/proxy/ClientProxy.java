package star191.f3.proxy;

import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import star191.f3.CoordHide;

public class ClientProxy extends CommonProxy{
    @Override
    public void preInit(FMLPreInitializationEvent event)
    {
        super.preInit(event);

    }

    @Override
    public void init(FMLInitializationEvent event)
    {
        super.init(event);

        if (FMLCommonHandler.instance().getEffectiveSide().isClient()){
            MinecraftForge.EVENT_BUS.register(new CoordHide());
        }
    }

    @Override
    public void postInit(FMLPostInitializationEvent event)
    {
        super.postInit(event);
    }
}
